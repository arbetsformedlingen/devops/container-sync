FROM registry.access.redhat.com/ubi8/skopeo:8.5-12


RUN mkdir /app
WORKDIR /app
COPY sync.yaml run.sh /app
RUN chmod +x /app/run.sh

CMD /app/run.sh
