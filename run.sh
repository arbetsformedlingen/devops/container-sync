#!/bin/bash

echo $NEXUS_PASSWORD | skopeo login -u $NEXUS_USER --password-stdin docker-images.jobtechdev.se
echo $DOCKER_PASSWORD | skopeo login -u $DOCKER_USER --password-stdin docker.io

skopeo sync --src yaml --dest docker sync.yaml docker-images.jobtechdev.se/mirror
