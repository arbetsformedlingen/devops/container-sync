# container-sync 

Syncing containers, primarily from docker.io to our internal registry docker-registry.

Containers to sync is controlled by `sync.yaml` file.
Add new images there and they will be synced next night to

`docker-images.jobtechdev.se/mirror/IMAGENAME`
Note that the path of the image is removed in the sync.

Docs about how to write `sync.yaml`can be found
[here](https://github.com/containers/skopeo/blob/main/docs/skopeo-sync.1.md).
